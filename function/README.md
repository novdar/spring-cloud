# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.1.9.RELEASE/maven-plugin/)
* [Function](https://cloud.spring.io/spring-cloud-function/)

### Additional Links
These additional references should also help you:

* [Various sample apps using Spring Cloud Function](https://github.com/spring-cloud/spring-cloud-function/tree/master/spring-cloud-function-samples)

# Running locally

## Application

To build and run with maven do the following:
```
mvn clean install
mvn spring-boot:run
```
Application is running on port 8090 by dafault.

## Access Service

You can view the API health/info endpoints:

[http://localhost:8090/actuator/health](http://localhost:8090/actuator/health)

[http://localhost:8090/actuator/info](http://localhost:8090/actuator/info)

## /factorial

#### request
```
method: POST
url: http://localhost:8090/api/factorial
header: "Content-Type": "text/plain"
requestBody: 8
```
#### response
```
status: 200 OK
responseBody: 40320
```

## /log

#### request
```
method: POST
url: http://localhost:8090/api/log
header: "Content-Type": "application/json"
requestBody: [{"name":"roman", "surname":"2.0"},{"name":"ivan", "surname":"alyonkin"}]
```
#### response
```
status: 202 Accepted
console log: Person(name=roman, surname=2.0)
             Person(name=ivan, surname=alyonkin)
```

## /uppercase

#### request
```
method: POST
url: http://localhost:8090/api/uppercase
header: "Content-Type": "application/json"
requestBody: [{"name":"roman", "surname":"2.0"},{"name":"ivan", "surname":"alyonkin"}]
```
#### response
```
status: 200 OK
responseBody: [
                  {
                      "name": "ROMAN",
                      "surname": "2.0"
                  },
                  {
                      "name": "IVAN",
                      "surname": "ALYONKIN"
                  }
              ]
```

## /roll

#### request
```
method: GET
url: http://localhost:8090/api/roll
```
#### response
```
status: 200 OK
responseBody: [
                  "Never gonna give you up",
                  "Never gonna let you down",
                  "Never gonna run around and desert you",
                  "Never gonna make you cry",
                  "Never gonna say goodbye",
                  "Never gonna tell a lie and hurt you"
              ]
```
