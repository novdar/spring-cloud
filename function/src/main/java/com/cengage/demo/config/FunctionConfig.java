package com.cengage.demo.config;

import com.cengage.demo.model.Person;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import reactor.core.publisher.Flux;

@Slf4j
@Configuration
public class FunctionConfig {

  @Bean
  public Consumer<Flux<Person>> log() {
    return flux -> flux.toStream()
        .map(Person::toString)
        .forEach(log::info);
  }

  @Bean
  public Function<Flux<Person>, Flux<Person>> uppercase() {
    return flux -> flux.map(person -> {
      person.setName(person.getName().toUpperCase());
      person.setSurname(person.getSurname().toUpperCase());
      return person;
    });
  }

  @Bean
  public Supplier<Flux<String>> roll() {
    return () -> Flux.just(
        "Never gonna give you up",
        "Never gonna let you down",
        "Never gonna run around and desert you",
        "Never gonna make you cry",
        "Never gonna say goodbye",
        "Never gonna tell a lie and hurt you");
  }
}
