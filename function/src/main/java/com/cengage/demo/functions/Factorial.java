package com.cengage.demo.functions;

import java.util.function.Function;
import org.apache.commons.math3.util.ArithmeticUtils;

public class Factorial implements Function<Integer, Long> {

  @Override
  public Long apply(Integer number) {
    return ArithmeticUtils.factorial(number);
  }
}
