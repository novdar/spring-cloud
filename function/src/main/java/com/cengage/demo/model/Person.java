package com.cengage.demo.model;

import lombok.Data;

@Data
public class Person {
  private String name;
  private String surname;
}
