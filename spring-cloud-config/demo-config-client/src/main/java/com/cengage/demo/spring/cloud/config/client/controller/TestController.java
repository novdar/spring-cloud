package com.cengage.demo.spring.cloud.config.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RefreshScope
@RestController
class TestController {

  @Value("${test.value}")
  private String testValue;

  @GetMapping("/test/value")
  public String getTestValue() {
    return testValue;
  }
}
