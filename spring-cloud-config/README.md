Practice for Spring Cloud Config

#### Rabbit MQ

Rabbit MQ is required by default to run config and client applications:

```
docker run -d --network=host rabbitmq:3-management-alpine
```

If you don't want to run spring cloud config with spring cloud bus features just remove followed dependencies:

```xml
    <!--./demo-config-server/pom.xml -->
    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
    </dependency>
    <!--./demo-config-client/pom.xml -->
    <dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-bus-amqp</artifactId>
    </dependency>
```